﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : FighterController {

	[Header("Genetic Stuff")]
	public GeneticData geneData;

	// Use this for initialization
	void Start () {
		// Get data component
		data = GetComponent<Fighter> ();
	}

	public override void DoOneTurn () {
		//  if it is my turn -- try to do attacks in order!

		if (data.isTurn) {
			for (int i = 0; i < data.actions.Count; i++) {
				data.actions [i].cooldownRemaining--;
			}

			if (!data.actions [0].DoAction (data.target.data)) {
				if (!data.actions [1].DoAction (data.target.data)) {
					if (!data.actions [2].DoAction (data.target.data)) {
						if (!data.actions [3].DoAction (data.target.data)) {
							// Then I can't act, so just do nothing
							//Debug.Log(name + " can't act! Doing nothing this turn!");
						}
					}
				}
			}

		}

		// End turn
		data.isTurn = false;
	}
}


