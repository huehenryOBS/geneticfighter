﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Action : MonoBehaviour {

	[Header("Base Data")]
	public string id;
	public string displayName;
	public float baseDamage;
	public float baseEnergy;
	public float baseCooldown;
	public bool causeGuard = false;
	public bool causeSelfGuard = false;
	public bool targetSelf;

	[Header("Current Data - Do not edit")]
	public float energyDamageModifier = 1;
	public float chanceOfChoice = 0.25f;
	public float cooldownRemaining = 0;

	[Header("Components - Do not edit")]
	public Fighter self;

	// Use this for initialization
	void Start () {
		self = GetComponent<Fighter> ();		
		cooldownRemaining = 0;
	}
	
	public bool DoAction (Fighter target) {
		self = GetComponent<Fighter> ();		

		// if we are not on cooldown 
		if (cooldownRemaining > 0) {
			//Debug.Log (name + " tries " + displayName + " but it is on cooldown!"); 
			return false;
		}

		// Check if we have enough energy
		float modifiedEnergy = baseEnergy * energyDamageModifier;
		if (self.energy < modifiedEnergy) {
			//Debug.Log (name + " tries " + displayName + " but does not have enough energy!"); 
			return false;
		}

		// Do action
		float modifiedDamage = baseDamage * energyDamageModifier;

		// If enemy is guarding, reduce 
		if (target.isGuarding) {
			modifiedDamage *= GameManager.instance.guardModifier;
		}

		// subtract from energy
		self.energy -= modifiedEnergy;

		// subtract from health
		if (targetSelf) {
			self.TakeDamage (modifiedDamage, self);
		} else {
			target.TakeDamage (modifiedDamage, self);
		}

		// If we set guards, set them
		if (causeGuard) {
			target.isGuarding = true;
		}

		if (causeSelfGuard) {
			self.isGuarding = true;
		} else {
			self.isGuarding = false;
		}

		// Set cooldown
		cooldownRemaining = baseCooldown;

		// Make sure we don't go over max energy and health
		self.hp = Mathf.Min(self.hp, self.maxHp);
		target.hp = Mathf.Min (target.hp, target.maxHp);
		self.energy = Mathf.Min(self.energy, self.maxEnergy);
		target.energy = Mathf.Min (target.energy, target.maxEnergy);

		// Display
		//Debug.Log (name + " uses " + displayName + " for "+ modifiedEnergy +"("+baseEnergy + "*"+ energyDamageModifier+")" + " energy and "+ modifiedDamage +"("+baseDamage + "*"+ energyDamageModifier+") hp!"); 

		// End turn
		self.isTurn = false;
		target.isTurn = true;
		return true;
	}
}
