﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fighter : MonoBehaviour {

	public string displayName = "";
	public float hp = 100;
	public float maxHp = 1000;
	public float energy = 100;
	public float maxEnergy = 100;
	public bool isGuarding;
	public bool isTurn;

	public List<Action> actions;
	public FighterController target;

	void Start () {
		Reset ();
	}


	public void Reset () {
		isGuarding = false;
		hp = maxHp;
		energy = maxEnergy;
	}

	public void TakeDamage (float amount, Fighter cause) {
		// Adjust if guarding
		amount *= GameManager.instance.guardModifier;

		// Subtract from health 
		hp -= amount;
	}

	public void AddAction ( Action actionToAdd ) {
		//Debug.Log ("Attempting to add:" + actionToAdd.id);


		if (actions == null) {
			//Debug.Log ("AI had no actons, creating empty set.");
			actions = new List<Action> ();
		}

		// Make a new component and add that one
		Action temp = GameManager.CopyComponent<Action> (actionToAdd, gameObject);
		//Debug.Log ("Adding Action "+temp.name+temp.displayName+temp.id);
		actions.Add (temp);
	}


}
