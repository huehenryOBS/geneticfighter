﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManager : MonoBehaviour {

	public static GameManager instance;

	[Header("UI Objects")]
	public Text topText;
	public Text turnText;
	public Text P1Stats;
	public Text P2Stats;

	[Header("Objects")]
	public FighterController playerOne;
	public FighterController playerTwo;

	[Header("Global Modifiers")]
	[Range(0,1)] public float guardModifier = 0.5f;

	[Header("Default Actions")]
	public Action defaultAction;
	public List<Action> actionsList;

	public bool playerOneTurn = true;


	// Use this for initialization
	void Awake () {
		instance = this;
	}

	void Start () {
	}

	public void Reset () {
		Debug.Log ("Fight Reset!");
		playerOneTurn = true;
	}

	// Update is called once per frame
	void Update () {
		UpdateUI();
	}

	void UpdateUI () {
		if (playerOne == null) {
			P1Stats.text = "NO PLAYER 1";
			turnText.text = "";
		} else {
		}

		if (playerTwo == null) {
			P1Stats.text = "NO PLAYER 2";
			turnText.text = "";
		} else { 
		}
				
	}
		
	public GeneticData SimulateFight ( GeneticData one, GeneticData two) {
		// Create AIControllers for each
		AIController p1 = GeneticManager.MakeAIFromGeneticData(one);
		AIController p2 = GeneticManager.MakeAIFromGeneticData(two);

		// Make those fight and return winner
		AIController winner = SimulateFight(p1, p2) as AIController;

		Destroy (p1.gameObject);
		Destroy (p2.gameObject);

		return winner.geneData;
	}

	public FighterController SimulateFight ( FighterController one, FighterController two) {
		// Make those fight
		int currentTurn = 0;

		// Set targets
		one.data.target = two;
		two.data.target = one;

		// Flip a coin as to who goes first
		if (Random.value > 0.5) {
			one.data.isTurn = true;
		} else {
			two.data.isTurn = true;
		}

		while (one.data.hp > 0 && two.data.hp > 0 && currentTurn < 99) {
			// Count turns - we only get 99 to stop endless loops
			currentTurn++;

			if (one.data.isTurn) {
				//Debug.Log ("Player1 Go");
				one.DoOneTurn ();
				one.data.isTurn = false;
				two.data.isTurn = true;
			} else if (two.data.isTurn) {
				//Debug.Log ("Player2 Go");
				two.DoOneTurn ();
				one.data.isTurn = true;
				two.data.isTurn = false;
			} else {
				//Debug.Log ("Nobody's Turn?");
			}
		}

		// Highest HP wins
		if (one.data.hp <= two.data.hp) {
			return two;
		} else {
			return one;
		}
	}


	public bool isAIPlayer (Fighter player) {
		AIController test = player.GetComponent<AIController> ();
		if (test != null) {
			return true;
		} else {
			return false;
		}
	}
		


	 public static T CopyComponent<T>(T original, GameObject destination) where T : Component
	 {
    	 System.Type type = original.GetType();
     	Component copy = destination.AddComponent(type);
     	System.Reflection.FieldInfo[] fields = type.GetFields();
     	foreach (System.Reflection.FieldInfo field in fields)
	     {
    	     field.SetValue(copy, field.GetValue(original));
    	 }
    	 return copy as T;
 	}
}
