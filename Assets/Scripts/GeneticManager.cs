﻿/*******************************************************
 * 6 Genes -- 0/2/4/6 is which action is action 0,1,2,3
 *            1/3/5/7 is the energymodifier for that action
 * *******************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneticManager : MonoBehaviour {

	public List<GeneticData> population;
	public int populationSize = 100;
	[Range(0,1)] public float mutationChance;
	public static int numberOfGenes = 8;
	public static int maxGeneValue = 10;
	public int numberOfGenerationsToTest = 1000;

	// Use this for initialization
	void Start () {

		// Clear data file:
		System.IO.File.WriteAllText("Populations.txt","");

		// Fill remainder of population with 100 random genetics
		while (population.Count < populationSize) {
			population.Add (RandomCreature ());
		}

		// Start the loop
		StartCoroutine ("Loop");
	}


	public IEnumerator Loop () {
		// For each generation
		for (int currentGeneration = 0; currentGeneration < numberOfGenerationsToTest; currentGeneration++) {
			// Test them for fitness
			Debug.Log("************** Testing For fitness - Generation #" + currentGeneration + "**************");
			yield return StartCoroutine("TestForFitness");

			// Sort our population by fitness
			population.Sort(delegate (GeneticData a, GeneticData b) {
				return(b.fitness.CompareTo(a.fitness));
			});

			//Print generation to data file
			AppendGenerationToFile ();

			// Breed the next generation
			Debug.Log("************** Breeding next generation. **************");
			population = NextGeneration(population);

			// One frame between generations
			yield return null;
		}
	}


	// Update is called once per frame
	void Update () {
		
	}


	public void AppendGenerationToFile () {
		string temp = "";
		temp += "************************ NEW GENERATION ************************\n";
		for (int i = 0; i < population.Count; i++) {
			temp += population[i].Display() + "\t|\t";
			if (i % 3 == 2) {
				temp += "\n";
			}
		}
		temp += "\n";
		System.IO.File.AppendAllText("Populations.txt",temp);
		//Debug.Log ("---");
	}

	public IEnumerator TestForFitness() {
		// Reset all fitness to zero
		for (int i = 0; i < population.Count; i++) {
			population [i].fitness = 0;
		}
			
		// For every creature
		foreach (GeneticData player1 in population) {			
			// Fight every creature
			foreach (GeneticData player2 in population) {
				// Don't fight yourself
				if (player1 == player2) {
					Debug.Log ("Fight Skipped:"+player1.Display() + " vs "+player2.Display());
					continue;
				}

				// Count wins
				if (GameManager.instance.SimulateFight (player1, player2) == player1) {
					player1.fitness++;
					Debug.Log("Fight:"+player1.Display() + " vs "+player2.Display() + "| Winner:"+player1.Display()); 
					} else {
					player2.fitness++;
					Debug.Log("Fight:"+player1.Display() + " vs "+player2.Display() + "| Winner:"+player2.Display()); 
					}
				// Frame draw after every fight
				//yield return null;
			}
		}

		// Frame draw after testing a generation
		yield return null;
	}

	public static AIController MakeAIFromGeneticData (GeneticData data) {
		//Debug.Log("Creating AI from Genes :"+data.Display());

		GameObject temp = new GameObject();
		temp.AddComponent<Fighter>();
		temp.name = "New AI";
		AIController cont = temp.AddComponent<AIController> ();
		cont.geneData = data;
		LoadActions (cont);
		return (cont);
	}

	public static GeneticData RandomCreature ( ) {
		GeneticData temp = new GeneticData ();
		temp.fitness = 0;
		for (int i = 0; i < numberOfGenes; i++) {
			temp.genes.Add (Random.Range (0, maxGeneValue));
		}
		return temp;
	}

	public static void LoadActions (AIController ai) {
		//Debug.Log ("LOADING ACTIONS");
		if (ai.geneData == null) {
			ai.geneData = new GeneticData ();
		}

		if (ai.geneData.genes == null) {
			ai.geneData.genes = new List<int> ();
		}

		ai.data = ai.gameObject.GetComponent<Fighter> ();

		if (ai.geneData.genes.Count > 0) {
			// Then apply genes
			//Debug.Log("Adding actions from genes!");
			for (int geneNum = 0; geneNum < GeneticManager.numberOfGenes; geneNum++ ) {
				int currentGene = ai.geneData.genes [geneNum];
				if (geneNum % 2 == 0) {
					//Debug.Log("Gene "+geneNum+" is even - adding action " + currentGene + ".");
					Action actionToAdd = GameManager.instance.actionsList [currentGene];
					ai.data.AddAction (actionToAdd);
				} else {
					float newModifier = ((float)currentGene / (float)GeneticManager.maxGeneValue) * 2;
					int actionGene = geneNum/2;
					//Debug.Log( "Gene " + geneNum + " is odd - changing modifier of action " + actionGene + " to " + newModifier + "("+currentGene+").");

					ai.data.actions [actionGene].energyDamageModifier = newModifier;
				}
			}
		} else {
			ai.data.AddAction (GameManager.instance.defaultAction);
			ai.data.AddAction (GameManager.instance.defaultAction);
			ai.data.AddAction (GameManager.instance.defaultAction);
			ai.data.AddAction (GameManager.instance.defaultAction);
		}
	}

	public List<GeneticData> NextGeneration(List<GeneticData> population) {
		
		List<GeneticData> newPop = new List<GeneticData>();
		// Start at the beginning
		int currentMember = 0;

		// Add our champion 
		Debug.Log ("Adding Champion to next generation: "+population[currentMember].Display());
		newPop.Add(population[currentMember]);

		// As long as we haven't filled our pop
		while (newPop.Count < populationSize) {
			// if our current member exists in the current data
			if (population.Count > currentMember) {
				// Then, breed it with every member better (lower in sort) than itself (and itself)!
				for (int i = 0; i <= currentMember; i++) {
					// Add the child to the new population
					newPop.Add(Breed(population[currentMember], population[i]));

					// If this puts us over, exit the for loop
					if (newPop.Count >= populationSize) {
						break;
					}
				}
			} else {
				// Otherwise, we bred everyone and our pop is still too small. Add a random. (This should never happen).
				newPop.Add(RandomCreature());
			}
			currentMember++;
		}

		return newPop;
	}


	public GeneticData Breed (GeneticData one, GeneticData two) {

		string breedingInfo = "";
		breedingInfo += "Breeding: "+one.Display() + " & "+two.Display() + " | ";

		GeneticData child = new GeneticData ();
		child.genes = new List<int>(new int[numberOfGenes]);

		float chanceParent1 = (1-mutationChance)/2;
		float chanceParent2 = (1-mutationChance)/2;

		// For every gene... 
		for (int i = 0; i < numberOfGenes; i++) {
			float randomNum = Random.value;
			if (randomNum < chanceParent1) {
				breedingInfo += "1";
				child.genes [i] = one.genes [i];
			} else if (randomNum < chanceParent1 + chanceParent2) {
				breedingInfo += "2";
				child.genes [i] = two.genes [i];
			}
			else {
				breedingInfo += "M";
				child.genes [i] = Random.Range (0, maxGeneValue);
			}
		}

		child.fitness = 0;
		breedingInfo += " | NewChild: "+child.Display();
		Debug.Log (breedingInfo);

		// return the child
		return child;
	}
}

[System.Serializable]
public class GeneticData {

	public List<int> genes = new List<int>();
	public float fitness;

	public string Display() {
		string temp = "[";
		for (int i = 0; i < genes.Count; i++) {
			temp += genes[i];
		}
		temp += "]:" + fitness;
		return temp;
	}
}



