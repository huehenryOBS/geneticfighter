﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : FighterController {

	public List<string> actions;


	// Use this for initialization
	void Start () {
		Action[] actions = GetComponents<Action> ();

		data = GetComponent<Fighter> ();

		for (int i = 0; i < actions.Length; i++) {
			if (actions [i] != null) {
				data.AddAction (actions [i]);
			} else {
				data.AddAction (GameManager.instance.defaultAction);
			}
		}
	}
	
	public override void DoOneTurn () {
		// If it is my turn
		if (data.isTurn) {
			// Check for inputs
			if (Input.GetKeyDown (KeyCode.Q)) {
				if (data.actions.Count > 0 && data.actions [0] != null) {
					if (data.actions [0].DoAction (data.target.data)) {
						for (int i = 0; i < data.actions.Count; i++) {
							data.actions [i].cooldownRemaining--;
						}
					};
				}
			}
			if (Input.GetKeyDown (KeyCode.W)) {
				if (data.actions.Count > 1 && data.actions [1] != null) {
					if (data.actions [1].DoAction (data.target.data)) {
						for (int i = 0; i < data.actions.Count; i++) {
							data.actions [i].cooldownRemaining--;
						}
					}
				}
			}

			if (Input.GetKeyDown (KeyCode.E)) {
				if (data.actions.Count > 2 && data.actions [2] != null) {
					if (data.actions [2].DoAction (data.target.data)) {
						for (int i = 0; i < data.actions.Count; i++) {
							data.actions [i].cooldownRemaining--;
						}
					}
				}
			}

			if (Input.GetKeyDown (KeyCode.R)) {
				if (data.actions.Count > 3 && data.actions [3] != null) {
					if (data.actions [3].DoAction (data.target.data)) {
						for (int i = 0; i < data.actions.Count; i++) {
							data.actions [i].cooldownRemaining--;
						}

					}
				}
			}
		}
	}

	public void DoTurn() {
		data.isTurn = true;
	}
}
